<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy guest your API!
|
*/


Route::group(['prefix' => '/v1','middleware' => ['secure']], function() {
	//Guests
	Route::get('/guests', array('as'=>'Guest List', 'uses'=>'GuestController@guests'));
    Route::get('/guests/{id}', array('as'=>'Guest Details', 'uses'=>'GuestController@getGuestDetail'));
    Route::post('/guests', array('as'=>'Create Guest', 'uses'=>'GuestController@createGuest'));
    Route::delete('/guests/{id}', array('as'=>'Delete Guest', 'uses'=>'GuestController@deleteGuest'));
    Route::put('/guests/{id}', array('as'=>'Guest Update', 'uses'=>'GuestController@updateGuest'));


	Route::post('/findGuestByAttr', array('as'=>'Guest Details', 'uses'=>'GuestController@findGuestByAttr'));
    Route::get('/getEditGuestDetail/{id}', array('as'=>'Guest Edit', 'uses'=>'GuestController@getEditGuestDetail'));


});
