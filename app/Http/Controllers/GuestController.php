<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Traits\ApiResponser;
use Mockery\Exception;
use Validator;
use App\Guest;


class GuestController extends Controller
{
	 use ApiResponser;

	public function __construct()
    {

    }
	// get all guests
	function guests(){
		$guests = Guest::all();
		return $this->successResponse($guests);
	}

	//get a single guest details
	function getGuestDetail($id){

		$guest = Guest::where('id', $id)->first();
		return $this->successResponse($guest);
	}

	//get a single guest details
	function findGuestByAttr(Request $request){
	    try {

	        $guest = Guest::where('guest_email', $request->guest_email)->first();
            return $this->successResponse($guest, Response::HTTP_ACCEPTED);

	    } catch(Exception $e) {
	        return $this->errorResponse($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }



	//insert new guests
	function createGuest(Request $request){
		//var_dump($request->all());die;
        $rule = [
            'guest_first_name' 	=> 'required|string',
            'guest_last_name' 	=> 'required|string',
            'guest_email' 		=> 'required|string|unique:guests'
        ];
        $validation = Validator::make($request->only(['guest_first_name','guest_last_name','guest_email']), $rule);

		if($validation->fails()){
			$return_arr = array('status' => 'error', 'msg' => 'Please fill all required field.');
			return $this->errorResponse('Validation Fails. ERROR!!! ',Response::HTTP_NOT_ACCEPTABLE);
		}
		else{

			$guest = Guest::create([
				'guest_first_name' 		=>  $request->input('guest_first_name'),
				'guest_last_name' 		=>  $request->input('guest_last_name'),
				'guest_email' 			=>  $request->input('guest_email'),
				'guest_company_name' 	=>  $request->input('guest_company_name'),
				'guest_company_reg_nr' 	=>  $request->input('guest_company_reg_nr'),
				'guest_company_vat' 	=>  $request->input('guest_company_vat'),
				'guest_company_address' =>  $request->input('guest_company_address'),
				'guest_company_contacts'=>  $request->input('guest_company_contacts')
			]);
			return $this->successResponse($guest, Response::HTTP_CREATED);
		}
	}




	//get details guest to edit
	function getEditGuestDetail($id){
		$guest = Guest::where('id', $id)->get();
		return $this->successResponse($guest);
	}

	function updateGuest(Request $request, $id){
		$rule = [
			'guest_id' 			=> 'required',
            'guest_first_name' 	=> 'required|string',
            'guest_last_name' 	=> 'required|string',
            'guest_email' 		=> 'required|string'
        ];
		$validation = Validator::make($request->only(['guest_id','guest_first_name','guest_last_name','guest_email']), $rule);

		if($validation->fails()){
			$return_arr = array('status' => 'error', 'msg' => 'Please fill all required field.');
			return $this->errorResponse('Validation Fails. ERROR!!! ',Response::HTTP_NOT_ACCEPTABLE);
		}
		else{
			$guest = Guest::find($request->input('guest_id'));

			$guest->guest_first_name 		= $request->input('guest_first_name');
			$guest->guest_last_name 		= $request->input('guest_last_name');
			$guest->guest_email 			= $request->input('guest_email');
			$guest->guest_company_name 		= $request->input('guest_company_name');
			$guest->guest_company_reg_nr	= $request->input('guest_company_reg_nr');
			$guest->guest_company_vat 		= $request->input('guest_company_vat');
			$guest->guest_company_address 	= $request->input('guest_company_address');
			$guest->guest_company_contacts 	= $request->input('guest_company_contacts');

			$guest = $guest->save();
			return $this->successResponse($guest, Response::HTTP_CREATED);
		}
	}

	function deleteGuest($id){
		$guest = Guest::find($id)->delete();
		return $this->successResponse($guest, Response::HTTP_CREATED);
	}

}
