-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.11-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table uss_bnb_guest.guests
CREATE TABLE IF NOT EXISTS `guests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `guest_first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guest_last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guest_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guest_company_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guest_company_reg_nr` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guest_company_vat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guest_company_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guest_company_contacts` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT 1 COMMENT '1:active,0:inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table uss_bnb_guest.guests: ~0 rows (approximately)
/*!40000 ALTER TABLE `guests` DISABLE KEYS */;
INSERT INTO `guests` (`id`, `guest_first_name`, `guest_last_name`, `guest_email`, `guest_company_name`, `guest_company_reg_nr`, `guest_company_vat`, `guest_company_address`, `guest_company_contacts`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'MomitTTT', 'Hasan', 'zsdfsddsd@dfgdf.com', NULL, NULL, NULL, NULL, NULL, 1, NULL, '2020-03-16 13:05:17', NULL),
	(2, 'Momit', 'Hasan', 'momit@dfgdf.com', NULL, NULL, NULL, NULL, NULL, 1, '2020-03-16 12:58:42', '2020-03-16 12:58:42', NULL),
	(3, 'MomitTTT', 'Hasan', 'zsdfsddsd@dfgdf.com', NULL, NULL, NULL, NULL, NULL, 1, '2020-03-16 13:04:20', '2020-03-16 13:20:35', '2020-03-16 13:20:35'),
	(5, 'sdfsdfsdfsdfsf', 'Hasan', 'sdfsdf@dfgdf.com', NULL, NULL, NULL, NULL, NULL, 1, '2020-03-16 13:19:57', '2020-03-16 13:19:57', NULL);
/*!40000 ALTER TABLE `guests` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
